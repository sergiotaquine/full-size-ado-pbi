// ==UserScript==
// @name         Full screen PBI
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Toggle full size PBI by adding a button on ADO
// @author       Sergio Taquine
// @match        https://dev.azure.com/lpl-sources/GDS-PROJECTS/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    if (window.top != window.self) { //-- Don't run on frames or iframes
        return;
    }

    const BUTTON_ID = "button-setfullscreen";
    const CLOSE_BUTTON_ID = "close-overloaded-bar-button"
    setTimeout(() => {
        const body = document.getElementsByClassName('form-grid')[0];
        const sideWrapper = document.getElementsByClassName('wrapping-container')[0];
        const mainSectionContainer = document.getElementsByClassName('section-container')[0];
        const mainSections = mainSectionContainer.getElementsByClassName('section');
        if (sideWrapper) {
            console.log("I'm well loaded")
            let overloadedBar = document.createElement('div');
            overloadedBar.style.height= '50px';
            overloadedBar.style.display= 'flex';
            overloadedBar.style.justifyContent= 'flex-end';
            overloadedBar.style.alignItems= 'center';
            overloadedBar.style.marginRight = '20px';
            overloadedBar.style.zIndex= '9000';

            let actionButton = document.createElement('button');
            actionButton.setAttribute("id", BUTTON_ID);
            actionButton.style.padding = '8px 16px';
            actionButton.style.borderRadius = '5px';
            actionButton.style.backgroundColor = 'violet';
            actionButton.style.zIndex = '9000';
            actionButton.style.fontFamily = "'Futura', 'Helvetica Neue', Helvetica, Arial, 'sans-serif'";
            actionButton.style.border= 'none';
            actionButton.innerHTML = 'Toggle full Size PBI';
            actionButton.style.cursor = 'pointer';

            let overloadedBarClosingButton = document.createElement('span');
            overloadedBarClosingButton.style.fontSize = '2rem';
            overloadedBarClosingButton.style.color = 'white';
            overloadedBarClosingButton.style.cursor = 'pointer';
            overloadedBarClosingButton.style.margin = '16px';
            overloadedBarClosingButton.innerHTML = 'X';

            overloadedBarClosingButton.setAttribute('id', CLOSE_BUTTON_ID);
            overloadedBar.prepend(overloadedBarClosingButton);
            overloadedBar.prepend(actionButton);

            body.prepend(overloadedBar);

            document.getElementById(BUTTON_ID).onclick = function() {

                const display = sideWrapper.style.display
                if (!display || display === 'inline-block') {
                    sideWrapper.style.display = 'none';
                    Array.from(mainSections).forEach((section) => {
                        section.style.width = '100%';
                    })
                } else {
                    sideWrapper.style.display = 'inline-block';
                    Array.from(mainSections).forEach((section) => {
                        section.style.width = '50%';
                    })
                }
            };

            document.getElementById(CLOSE_BUTTON_ID).onclick = function() {
                 overloadedBar.remove();
            };
        }
    }, 3000)
})();
